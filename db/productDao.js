const conn = require('./index');
const collName='product';
const logger=require('../logger');

function getproducts(name=null,type=null){
    let db = conn.db;
    return new Promise((resolve,reject)=>{
      var param;
        var coll=db.collection(collName);
        if(!(name && type )){
            param={'age':age,'phone':phone};
        }else if(!name){
            param={'name':name};
        }else if(!type ){
            param={'type':type};
        }else{
        param={};
        }
       coll .find(param).toArray((err,res)=>{
            if(err){
                reject(err);
            }else{
                logger.log('info',{'message':res});
                resolve(res);
            }
        });
    });
}

function getproduct(id) {
    logger.log('info','received id at dao ',{'message':id});
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).findOne({'_id':Number(id)},(err,res)=>{
            if(err){
                logger.log('error',{'message':err});
                reject(err);
            }else{
                logger.log('info','Returning response is ',{'message':JSON.stringify(res)});
                resolve(res);
            }
        }
        );
    }
    );
}
function postproduct(product) {
    console.log('received product',JSON.stringify(product));
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).insertOne(product,(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}
function putproduct(product,id) {
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).updateOne({'_id':Number(id)},
        { $set: product }    
        ,(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}

function deleteproduct(id) {
    console.log('delete product',(id));
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).remove({'_id':Number(id)},(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}

module.exports={
    getproducts,
     getproduct,
     postproduct,
     putproduct,
     deleteproduct
}
