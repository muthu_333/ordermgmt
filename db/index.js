const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/ordermgmt';
let conn={};
 
  function initialize() {     
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,(err,client)=>{
                if(err){
                    console.log('error in db conn');
                    reject(err);
                }else{
                    conn.db = client.db('ordermgmt');
                    console.log('got con');
                    resolve(conn.db);
                }
            });
        });   
    }
initialize();

module.exports = conn;
