const conn = require('./index');
const collName='customer';
const logger=require('../logger');

function getCustomers(name=null,phone=null){
    let db = conn.db;
    return new Promise((resolve,reject)=>{
      var param;
        var coll=db.collection(collName);
        if(!(name && phone )){
            param={'name':name,'phone':phone};
        }else if(!name){
            param={'name':name};
        }else if(!phone ){
            param={'phoneNumber':phone};
        }else{
        param={};
        }
       coll .find(param).toArray((err,res)=>{
            if(err){
                reject(err);
            }else{
                logger.log('info',{'message':res});
                resolve(res);
            }
        });
    });
}

function getCustomer(id) {
    logger.log('info','received id at dao ',{'message':id});
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).findOne({'_id':Number(id)},(err,res)=>{
            if(err){
                logger.log('error',{'message':err});
                reject(err);
            }else{
                logger.log('info','Returning response is ',{'message':JSON.stringify(res)});
                resolve(res);
            }
        }
        );
    }
    );
}
function postCustomer(customer) {
    console.log('received customer',JSON.stringify(customer));
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).insertOne(customer,(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}
function putCustomer(customer,id) {
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).updateOne({'_id':Number(id)},
        { $set: customer }    
        ,(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}

function deleteCustomer(id) {
    console.log('delete customer',(id));
    let db = conn.db;
    return new Promise((resolve,reject)=>
    {
        db.collection(collName).remove({'_id':Number(id)},(err,res)=>{
            if(err){
                console.log('err',err);
                reject(err);
            }else{
                resolve(res);
            }
        }
        );
    }
    );
}

module.exports={
    getCustomers,
     getCustomer,
     postCustomer,
     putCustomer,
     deleteCustomer
}
