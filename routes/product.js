const express = require('express');
const router = express.Router();
const productdao=require('../db/productDao');
const logger=require('../logger');


router.get('/',(req,res)=>{
    productdao.getproducts(req.query.name,req.query.type,req.query)
    .then((result)=>{
        res.send((result));
    })
    .catch((err)=>{res.send(err)});
});


router.get('/:id',(req,res)=>{
    logger.log('info', 'received val is ', { message: req.params.id });
    productdao.getproduct(req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

router.post('/',(req,res)=>{
    logger.log('info', 'received body is ', { message: req.body });
    productdao.postproduct(req.body)
    .then((result)=>res.send(result.ops))
    .catch(err=>res.send(err));
});

router.put('/:id',(req,res)=>{
    logger.log('info', 'received body is ', { message: req.body });
    productdao.putproduct(req.body,req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

router.delete('/:id',(req,res)=>{
    logger.log('info', 'received val is ', { message: req.params.id });
    productdao.deleteproduct(req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

module.exports=router;