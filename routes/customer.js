const express = require('express');
const router = express.Router();
const customerdao=require('../db/customerDao');
const logger=require('../logger');


router.get('/',(req,res)=>{
    customerdao.getCustomers(req.query.name,req.query.phone)
    .then((result)=>{
        res.send((result));
    })
    .catch((err)=>{res.send(err)});
});


router.get('/:id',(req,res)=>{
    logger.log('info', 'received val is ', { message: req.params.id });
    customerdao.getCustomer(req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

router.post('/',(req,res)=>{
    logger.log('info', 'received body is ', { message: req.body });
    customerdao.postCustomer(req.body)
    .then((result)=>res.send(result.ops))
    .catch(err=>res.send(err));
});

router.put('/:id',(req,res)=>{
    logger.log('info', 'received body is ', { message: req.body });
    customerdao.putCustomer(req.body,req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

router.delete('/:id',(req,res)=>{
    logger.log('info', 'received val is ', { message: req.params.id });
    customerdao.deleteCustomer(req.params.id)
    .then((result)=>res.send(result))
    .catch(err=>res.send(err));
});

module.exports=router;